function [dxdt] = system_model(t,x)
A = [0 1 0;0 -0.8060 1;0 -9.1486 -4.59];
A_ref = [0    1.0000         0;
   -0.1789   -0.8675    0.9884;
  -20.5271  -16.2022   -5.9207];
B = [0;-0.04;-4.59];
B_ref = [-1;0;0];

r = input_function(t);

Kx = [-4.4721   -1.5367   -0.2899];

u = -Kx*x(1:3);

dxdt(1:3) = A*x(1:3) + B*u + B_ref * r;
dxdt(4:6) = A_ref*x(4:6) + B_ref * r;
dxdt = dxdt';
end

