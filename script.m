
x0 = [0;2;2;1;1;1]; % e_yi,a,q
tspan = [0 10];
[t,x] = ode45(@system_model,tspan, x0);
r = zeros(length(t),1);
for i=1:length(t)
    r(i) = input_function(t(i));
end

figure
plot(t,x(:,2))
hold on
plot(t,x(:,5))
hold on
plot(t,r)
legend('Output','Reference output','Reference input')