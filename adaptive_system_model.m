function [dxdt] = adaptive_system_model(t,x,P)

% e,a,q, e_ref, a_ref, q_ref, kx1,2,3

A = [0 1 0;0 -0.8060 1;0 -9.1486 -4.59];
A_ref = [0    1.0000         0;
   -0.1789   -0.8675    0.9884;
  -20.5271  -16.2022   -5.9207];
B = [0;-0.04;-4.59];

B_ref = [-1;0;0];

D = 0.5;
ka = 0.5*A(3,2);
kq = 0.5*A(3,3);

Gamma_x = [1 0 0; 0 1 0; 0 0 1];

f = ka*x(2) + kq*x(3);
r = input_function(t);

% K_x = [-4.4721   -1.5367   -0.2899];

% 
Kx = x(7:9);

% Kx = [8.9442   7.6477   2.8748]';

u = Kx'*x(1:3);

error = x(1:3) - x(4:6); % x - xref

dxdt(1:3) = A*x(1:3) + D*B*(u + f) + B_ref * r;
dxdt(4:6) = A_ref*x(4:6) + B_ref * r;
dxdt(7:9) = -Gamma_x * x(1:3) * error' * P * B;

dxdt = dxdt';
end

