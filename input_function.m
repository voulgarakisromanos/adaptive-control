function [u] = input_function(t)
    if t<1
        u=0;
    elseif t<10
        u=0.5;
    elseif t<22
        u=0;
    elseif t<32
        u=-0.5;
    elseif t<45
        u=0;
    elseif t<55
        u=1;
    elseif t<65
        u=0;
    elseif t<75
        u=-1;
    elseif t<85
        u=0;
    elseif t<95
        u=0.5;
    else
        u=0;
    end
end

