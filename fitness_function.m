function cost = fitness_function(p)
x0 = [0;2;2;0;1;1;10;10;10]; % e_yi,a,q, kx1:3
tspan = [0 10];
A_ref = [0	1	0;
-0.178900000000000	-0.867500000000000	0.988400000000000;
-20.5271000000000	-16.2022000000000	-5.92070000000000];

% Q = diag(q);
% P = lyap(A_ref,Q);
P = diag(p);
[t,x] = ode45(@(t,x)adaptive_system_model(t,x,P),tspan, x0);
cost = sum((x(1:3) - x(4:6)).^2);
end

