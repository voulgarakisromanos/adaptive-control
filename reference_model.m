function [dxdt] = reference_model(t,x)
A_ref = [0    1.0000         0;
   -0.1789   -0.8675    0.9884;
  -20.5271  -16.2022   -5.9207];

B_ref = [-1;0;0];

r = input_function(t);

dxdt = A_ref*x + B_ref * r;

end

