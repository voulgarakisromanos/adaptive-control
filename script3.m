
x0 = [0;2;2;0;1;1;10;10;10]; % e_yi,a,q, kx1:3
tspan = [0 100];

P = diag([3 96 96]);

[t,x] = ode45(@(t,x)adaptive_system_model(t,x,P),tspan, x0);
r = zeros(length(t),1);
for i=1:length(t)
    r(i) = input_function(t(i));
end

figure
plot(t,x(:,2))
hold on
plot(t,x(:,5))
hold on
plot(t,r)
legend('Output','Reference output','Reference input')

figure
plot(t,x(:,7))
hold on
plot(t,x(:,8))
hold on
plot(t,x(:,9))
legend('K1','K2','K3')

%Kideal = -8.9442   -7.6477   -2.8748